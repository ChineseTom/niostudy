package com.tom.niostudy.handler;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintStream;
import java.net.Socket;

/**
 * @author WangTao
 */
public class ClientHandler implements Runnable {


    private Socket socket;

    private boolean flag = true;

    public ClientHandler() {

    }

    public ClientHandler(Socket socket) {
        this.socket = socket;
    }

    @Override
    public void run() {
        System.out.println("新客户务端:" + socket.getInetAddress()
                +"===port:"+socket.getPort());
        PrintStream ps = null;
        BufferedReader br = null;
        try {
            ps = new PrintStream(socket.getOutputStream());
            br = new BufferedReader(new InputStreamReader(socket.getInputStream()));
            do{
                String s = br.readLine();
                if ("bye".equalsIgnoreCase(s)){
                    flag = false;
                    //给客户端返回bye
                    ps.println("bye");
                }else {
                    System.out.println(s);
                    ps.println("回送===长度:"+s.length());
                }
            }while (flag);

        }catch (IOException e){
            e.printStackTrace();
        }finally {
            if (ps != null){
                ps.close();
            }
            if (br != null){
                try {
                    br.close();
                }catch (IOException e){
                    e.printStackTrace();
                }
            }
            try {
                socket.close();
            }catch (IOException e){
                e.printStackTrace();
            }
            System.out.println("客户端已关闭:" + socket.getInetAddress()+"===ip"+socket.getPort());
        }

    }
}
