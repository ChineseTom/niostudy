package com.tom.niostudy.handler;
import	java.util.logging.Logger;
import	java.io.IOException;
import java.net.DatagramPacket;
import	java.net.DatagramSocket;
import com.tom.niostudy.hardward.Device;
import com.tom.niostudy.msg.MessageCreator;

import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.List;
import	java.util.concurrent.CountDownLatch;

/**
 * @author WangTao
 */


public class ListenHandler extends Thread {


    private static final org.slf4j.Logger logger = LoggerFactory.getLogger(ListenHandler.class);


    private final int lisentPort;

    private final CountDownLatch latch;

    private boolean done = false;


    //局域网所有的设备
    private final List<Device> devices = new ArrayList<>();

    private DatagramSocket ds = null;

    public ListenHandler(int lisentPort,CountDownLatch latch) {
        super();
        this.lisentPort = lisentPort;
        this.latch = latch;
    }

    @Override
    public void run() {
        super.run();
        //通知已经启动
        latch.countDown();

        try {
            ds = new DatagramSocket(lisentPort);

            while (!done){
                final byte[] buf = new byte[1024];
                DatagramPacket receive = new DatagramPacket(buf,buf.length);
                ds.receive(receive);
                String ip = receive.getAddress().getHostAddress();
                int port = receive.getPort();
                int dataLen = receive.getLength();
                String data = new String(receive.getData(),0,dataLen);
                System.out.println("UDPSearcher receive from ip: ip\t"+ip
                        +"\tport:"+port+"\t:data:"+data);

                String sn = MessageCreator.parseSn(data);
                if (sn != null){
                    Device device = new Device(port,ip,sn);
                    devices.add(device);
                }
            }
        }catch (IOException e){
            logger.error("{}",e.fillInStackTrace());
        }finally {
            close();
        }
        System.out.println("已经结束");
    }

    public List<Device> getDevicesAndClose(){
        done = true;
        close();
        return devices;
    }


    private  void close(){
        if (ds != null){
            ds.close();
            ds = null;
        }
    }


}
