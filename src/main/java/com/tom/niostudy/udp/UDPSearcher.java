package com.tom.niostudy.udp;
import java.util.List;
import	java.util.concurrent.CountDownLatch;



import com.tom.niostudy.handler.ListenHandler;
import com.tom.niostudy.hardward.Device;
import com.tom.niostudy.msg.MessageCreator;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;


/**
 * @author WangTao
 */
public class UDPSearcher {

    private final static int LISTEN_PORT = 3000;

    private final static String BROADCAST_IP = "255.255.255.255";

    public static void main(String[] args) throws Exception {
        System.out.println("UDPSearcher start");
        ListenHandler listenHandler = listen();
        sendBroad();
        System.in.read();
        List<Device> devices = listenHandler.getDevicesAndClose();
        for (Device device : devices){
            System.out.println("Device:"+device);
        }
        System.out.println("finished");
    }

    private static ListenHandler listen() throws InterruptedException{
        System.out.println("start listen");
        CountDownLatch latch = new CountDownLatch(1);
        ListenHandler listenHandler = new ListenHandler(LISTEN_PORT,latch);
        listenHandler.start();
        latch.await();
        return listenHandler;
    }

    private static void sendBroad() throws IOException{
        System.out.println("发送广播");
        //接受搜索方
        DatagramSocket socket = new DatagramSocket();

        String requestData = MessageCreator.buildWithPort(LISTEN_PORT);
        byte[] requestDataBytes = requestData.getBytes();
        // 直接构建packet
        DatagramPacket packet = new DatagramPacket(requestDataBytes,0,
                requestDataBytes.length);
        //关闭
        packet.setAddress(InetAddress.getByName(BROADCAST_IP));
        packet.setPort(2000);
        socket.send(packet);
        socket.close();
        System.out.println("发送广播已完成");
    }
}
