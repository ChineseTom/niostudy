package com.tom.niostudy.udp;


import com.tom.niostudy.msg.MessageCreator;
import com.tom.niostudy.util.UUIDUtil;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.util.UUID;

/**
 * @author WangTao
 */
public class ProviderHandler extends Thread {

    private final String sn;

    private volatile boolean done = false;

    private DatagramSocket datagramSocket = null;


    public ProviderHandler(String sn){
        super();
        this.sn = sn;
    }

    @Override
    public void run() {
        super.run();

        System.out.println("开始=============");
        //监听2000端口
        try {
            datagramSocket = new DatagramSocket(2000);
            while (!done){
                final byte[] buf = new byte[1024];
                DatagramPacket ds = new DatagramPacket(buf,buf.length);
                datagramSocket.receive(ds);

                String ip = ds.getAddress().getHostAddress();
                int port = ds.getPort();
                int dataLen = ds.getLength();

                String data = new String(ds.getData(),0,dataLen);
                System.out.println("UDPProvider receive from ip: ip\t"+ip
                        +"\tport:"+port+"\t:data:"+data);



                int responsePort = MessageCreator.parsePort(data);
                if (responsePort != -1){
                    String s = MessageCreator.buildWithSn(sn);
                    System.out.println(s);
                    DatagramPacket packet = new DatagramPacket(s.getBytes(),0
                            ,s.length(),
                            ds.getAddress(),
                            responsePort);
                    datagramSocket.send(packet);
                }

            }

        }catch (IOException e){
            e.printStackTrace();
        }finally {
            close();
        }

        System.out.println("结束=============");

    }

    private void exit(){
        done = true;
        close();
    }

    private  void close(){
        if (datagramSocket != null){
            datagramSocket.close();
            datagramSocket = null;
        }
    }

    public static void main(String[] args) throws IOException {
        String sn = UUIDUtil.getUUID();
        ProviderHandler handler = new ProviderHandler(sn);
        handler.start();
        System.in.read();
        handler.exit();
    }
}
