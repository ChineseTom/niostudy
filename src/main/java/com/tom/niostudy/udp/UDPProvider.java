package com.tom.niostudy.udp;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;

/**
 * @author WangTao
 */
public class UDPProvider {

    public static void main(String[] args) throws IOException {

        System.out.println("开始=============");
        //接受数据端口
        DatagramSocket socket = new DatagramSocket(2000);

        byte[] receBuf = new byte[1024];
        DatagramPacket packet = new DatagramPacket(receBuf, receBuf.length);
        socket.receive(packet);
        System.out.println("port:"+packet.getPort());
        String receStr = new String(packet.getData(), 0 , packet.getLength());
        System.out.println("receStr:" + receStr);

        String s = "Recieve data with length:"+receStr.length();
        packet = new DatagramPacket(s.getBytes(),0
                ,s.length(),packet.getAddress(),packet.getPort());
        socket.send(packet);
        System.out.println("结束=============");
        socket.close();

    }
}
