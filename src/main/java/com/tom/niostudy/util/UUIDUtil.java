package com.tom.niostudy.util;

import java.util.UUID;

/**
 * @author WangTao
 */
public class UUIDUtil {

    public static String getUUID() {
        return UUID.randomUUID().toString().replace("-","");
    }
}
