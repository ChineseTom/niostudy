package com.tom.niostudy.udpandtcp.server;

import com.tom.niostudy.udpandtcp.constants.TcpConstants;
import com.tom.niostudy.udpandtcp.provider.ServerProvider;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author WangTao
 */
public class Server {
    private static final Logger logger = LoggerFactory.getLogger(Server.class);

    public static void main(String[] args) {
        TcpServer tcpServer = new TcpServer(TcpConstants.PORT_SERVER);
        if (!tcpServer.start()){
            System.out.println("tcp server start error");
        }
        ServerProvider.start(TcpConstants.PORT_SERVER);
        try {
            System.in.read();
        }catch (Exception e){
            logger.error("exception:{}",e.getMessage());
        }
        ServerProvider.stop();
        tcpServer.stop();
    }
}
