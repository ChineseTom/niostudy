package com.tom.niostudy.udpandtcp.server;

import com.tom.niostudy.udpandtcp.listen.ClientListener;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;

/**
 * @author WangTao
 */
public class TcpServer {
    private final Logger logger = LoggerFactory.getLogger(TcpServer.class);
    private final int port;

    private ClientListener clientListener;

    public TcpServer(int port) {
        this.port = port;
    }

    public boolean start(){
        try {
            clientListener = new ClientListener(port);
            clientListener.start();
        }catch (IOException e){
            logger.error("can't create listener");
            return false;
        }
        return true;
    }

    public void stop() {
        if (clientListener != null){
            clientListener.exit();
        }
    }
}
