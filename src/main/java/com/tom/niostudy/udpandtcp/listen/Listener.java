package com.tom.niostudy.udpandtcp.listen;

import com.tom.niostudy.udpandtcp.client.bean.ServerInfo;
import com.tom.niostudy.udpandtcp.constants.UdpConstants;
import com.tom.niostudy.util.ByteUtils;

import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.nio.ByteBuffer;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.CountDownLatch;

/**
 * @author WangTao
 */
public class Listener extends Thread {
    private final int listenPort;
    private final CountDownLatch startDownLatch;
    private final CountDownLatch receiveDownLatch;
    private final List<ServerInfo> serverInfoList = new ArrayList<>();
    private final byte[] buffer = new byte[128];
    private final int minLen = UdpConstants.HEAD.length + 2 + 4;
    private boolean done = false;
    private DatagramSocket ds = null;

    public Listener(int listenPort, CountDownLatch startDownLatch, CountDownLatch receiveDownLatch) {
        super();
        this.listenPort = listenPort;
        this.startDownLatch = startDownLatch;
        this.receiveDownLatch = receiveDownLatch;
    }

    @Override
    public void run() {
        super.run();

        // 通知已启动
        startDownLatch.countDown();
        try {
            // 监听回送端口
            ds = new DatagramSocket(listenPort);
            // 构建接收实体
            DatagramPacket receivePack = new DatagramPacket(buffer, buffer.length);

            while (!done) {
                // 接收
                ds.receive(receivePack);

                // 打印接收到的信息与发送者的信息
                // 发送者的IP地址
                String ip = receivePack.getAddress().getHostAddress();
                int port = receivePack.getPort();
                int dataLen = receivePack.getLength();
                byte[] data = receivePack.getData();
                boolean isValid = dataLen >= minLen
                        && ByteUtils.startsWith(data, UdpConstants.HEAD);

                System.out.println("UDPSearcher receive form ip:" + ip
                        + "\tport:" + port + "\tdataValid:" + isValid);

                if (!isValid) {
                    // 无效继续
                    continue;
                }

                ByteBuffer byteBuffer = ByteBuffer.wrap(buffer, UdpConstants.HEAD.length, dataLen);
                final short cmd = byteBuffer.getShort();
                final int serverPort = byteBuffer.getInt();
                if (cmd != 2 || serverPort <= 0) {
                    System.out.println("UDPSearcher receive cmd:" + cmd + "\tserverPort:" + serverPort);
                    continue;
                }

                String sn = new String(buffer, minLen, dataLen - minLen);
                ServerInfo info = new ServerInfo(serverPort, ip, sn);
                serverInfoList.add(info);
                // 成功接收到一份
                receiveDownLatch.countDown();
            }
        } catch (Exception ignored) {
        } finally {
            close();
        }
        System.out.println("UDPSearcher listener finished.");
    }

    private void close() {
        if (ds != null) {
            ds.close();
            ds = null;
        }
    }

    public List<ServerInfo> getServerAndClose() {
        done = true;
        close();
        return serverInfoList;
    }
}
