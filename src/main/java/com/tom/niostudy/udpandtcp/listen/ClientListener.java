package com.tom.niostudy.udpandtcp.listen;

import com.tom.niostudy.udpandtcp.handler.ClientHandler;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;

/**
 * @author WangTao
 */
public class ClientListener extends Thread {

    private ServerSocket serverSocket;

    private boolean done = false;

    public ClientListener(int port) throws IOException{
        serverSocket = new ServerSocket(port);
        System.out.println("服务器信息：" + serverSocket.getInetAddress() + " P:" + serverSocket.getLocalPort());
    }

    @Override
    public void run() {
        super.run();
        System.out.println("服务器准备就绪～");
        do{
            // 得到客户端
            Socket client;
            try {
                client = serverSocket.accept();
            } catch (IOException e) {
                continue;
            }
            // 客户端构建异步线程
            //启用多个线程进行监听客户端请求
            ClientHandler clientHandler = new ClientHandler(client);
            // 启动线程
            clientHandler.start();
        }while (!done);
        System.out.println("服务器已关闭！");
    }

    public void exit() {
        done = true;
        try {
            serverSocket.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
