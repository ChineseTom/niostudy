package com.tom.niostudy.udpandtcp.constants;

/**
 * @author WangTao
 */
public class UdpConstants {

    //通用udp头部
    public static byte [] HEAD =new byte[] {7,7,7,7,7,7,7,7};

    //服务器端udp接受端口
    public static int PORT_SERVER = 30201;
    //客户端回送消息
    public static final int PORT_CLIENT_RESPONSE = 30202;
}
