package com.tom.niostudy.udpandtcp.client;

import com.tom.niostudy.udpandtcp.client.bean.ServerInfo;
import com.tom.niostudy.udpandtcp.provider.ClientSearch;

import java.io.IOException;

/**
 * @author WangTao
 */
public class Client {
    public static void main(String[] args) {
        ServerInfo info = ClientSearch.searchServer(10000);
        System.out.println("Server:" + info);
        if (info != null){
            try {
                TcpClient.linkWith(info);
            }catch (IOException e){
                e.printStackTrace();
            }
        }
    }
}
