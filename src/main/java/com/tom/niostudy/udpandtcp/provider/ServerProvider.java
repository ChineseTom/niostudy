package com.tom.niostudy.udpandtcp.provider;

import com.tom.niostudy.udpandtcp.handler.ServerHandler;
import com.tom.niostudy.util.UUIDUtil;

/**
 * @author WangTao
 */
public class ServerProvider {
    private static ServerHandler handler;

    public static void start(int port){
        stop();
        String uid = UUIDUtil.getUUID();
        handler = new ServerHandler(uid, port);
        handler.start();
    }

    public static void stop() {
        if (handler != null){
            handler.exit();
            handler = null;
        }
    }
}
