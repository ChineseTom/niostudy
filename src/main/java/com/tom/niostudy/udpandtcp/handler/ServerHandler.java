package com.tom.niostudy.udpandtcp.handler;


import com.tom.niostudy.udpandtcp.constants.UdpConstants;
import com.tom.niostudy.util.ByteUtils;

import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.nio.ByteBuffer;


/**
 * @author WangTao
 */
public class ServerHandler extends Thread {

    private final byte[] sn;
    private final int port;
    private volatile boolean done = false;
    private DatagramSocket ds = null;
    // 存储消息的Buffer
    final byte[] buffer = new byte[128];

    public ServerHandler(String sn, int port) {
        super();
        this.sn = sn.getBytes();
        this.port = port;
    }



    @Override
    public void run() {
        System.out.println("upd server start");
        try{
            ds = new DatagramSocket(UdpConstants.PORT_SERVER);
            DatagramPacket recievePacket = new DatagramPacket(buffer, buffer.length);
            while (!done){
                ds.receive(recievePacket);
                String clientIp = recievePacket.getAddress().getHostAddress();
                int clientPort = recievePacket.getPort();
                byte[] data= recievePacket.getData();
                int clientDataLen = recievePacket.getLength();
                //判断是否有数据
                boolean isValid = clientDataLen >= (UdpConstants.HEAD.length + 2 + 4)
                        && ByteUtils.startsWith(data, UdpConstants.HEAD);

                System.out.println("ServerProvider receive form ip:" + clientIp
                        + "\tport:" + clientPort + "\tdataValid:" + isValid);

                if (!isValid) {
                    // 无效继续
                    continue;
                }
                // head(固定长度)+cmd(short类型)+port(int类型 32位长度)
                // 解析命令与回送端口
                int index = UdpConstants.HEAD.length;
                short cmd = (short) ((data[index++] << 8) | (data[index++] & 0xff));
                //将byte数组转换为int类型
                int responsePort = (((data[index++]) << 24) |
                        ((data[index++] & 0xff) << 16) |
                        ((data[index++] & 0xff) << 8) |
                        ((data[index] & 0xff)));

                // 判断合法性
                if (cmd == 1 && responsePort > 0) {
                    // 构建一份回送数据
                    ByteBuffer byteBuffer = ByteBuffer.wrap(buffer);
                    byteBuffer.put(UdpConstants.HEAD);
                    byteBuffer.putShort((short) 2);
                    //tcp port
                    byteBuffer.putInt(port);
                    byteBuffer.put(sn);
                    int len = byteBuffer.position();
                    // 直接根据发送者构建一份回送信息
                    DatagramPacket responsePacket = new DatagramPacket(buffer,
                            len,
                            recievePacket.getAddress(),
                            responsePort);
                    ds.send(responsePacket);
                    System.out.println("ServerProvider response to:" + clientIp + "\tport:" + responsePort + "\tdataLen:" + len);
                } else {
                    System.out.println("ServerProvider receive cmd nonsupport; cmd:" + cmd + "\tport:" + port);
                }
            }

        }catch (Exception e){
            e.printStackTrace();
        }finally {
            close();
        }
    }

    private void close() {
        if (ds != null) {
            ds.close();
            ds = null;
        }
    }

    /**
     * 提供结束
     */
    public void exit() {
        done = true;
        close();
    }
}
