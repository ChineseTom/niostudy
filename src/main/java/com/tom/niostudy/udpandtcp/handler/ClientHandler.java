package com.tom.niostudy.udpandtcp.handler;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintStream;
import java.net.Socket;

/**
 * @author WangTao
 */
public class ClientHandler extends Thread {
    private final Logger logger = LoggerFactory.getLogger(ClientHandler.class);

    private Socket socket;
    private boolean flag = true;

    private static final String BYE = "bye";

    public ClientHandler(Socket socket){
        this.socket = socket;
    }

    @Override
    public void run() {
        super.run();
        System.out.println("新客户端连接：" + socket.getInetAddress() +
                " P:" + socket.getPort());
       try {
           PrintStream ps = new PrintStream(socket.getOutputStream());
           BufferedReader br = new BufferedReader(
                   new InputStreamReader(socket.getInputStream()));
           do{
               String s=br.readLine();
               if (BYE.equalsIgnoreCase(s)) {
                   flag = false;
                   ps.print(BYE);
               }else{
                   System.out.println(s);
                   ps.println("回送：" + s.length());
               }
           }while (flag);
           ps.close();
           br.close();
       }catch (Exception e){
           logger.error("exception");
       }finally {
           try {
               socket.close();
           } catch (IOException e) {
               logger.error("socket 关闭错误");
           }
       }
        System.out.println("客户端已退出：" + socket.getInetAddress() +
                " P:" + socket.getPort());
    }
}
