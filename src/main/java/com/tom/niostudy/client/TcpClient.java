package com.tom.niostudy.client;

import java.io.*;
import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.net.Socket;

public class TcpClient {

    public static void main(String[] args)  throws IOException {
        Socket socket = new Socket();
        socket.setSoTimeout(3000);
        socket.connect(new InetSocketAddress(InetAddress.getLocalHost(),8890),3000);

        System.out.println("已经建立连接");
        System.out.println("客户端:" + socket.getLocalAddress()+"===ip"+socket.getLocalPort());
        System.out.println("服务端:" + socket.getInetAddress()+"===ip"+socket.getPort());
        todo(socket);

        socket.close();
        System.out.println("客户端已经退出");
    }

    private static void todo(Socket socket) throws IOException{
        InputStream inputStream = System.in;
        BufferedReader br = new BufferedReader(new InputStreamReader(inputStream));

        InputStream is = socket.getInputStream();
        BufferedReader reader = new BufferedReader(new InputStreamReader(is));

        OutputStream os = socket.getOutputStream();
        PrintStream ps = new PrintStream(os);

        boolean flag = true;
        do{
            String s = br.readLine();
            ps.println(s);
            //从服务器读取一行数据
            String echo = reader.readLine();
            if ("bye".equals(echo)){
                flag = false;
            }else {
                System.out.println(echo);
            }
        }while (flag);

    }
}
