package com.tom.niostudy.client;

import com.tom.niostudy.handler.NioClientHandler;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.nio.channels.SelectionKey;
import java.nio.channels.Selector;
import java.nio.channels.SocketChannel;
import java.nio.charset.Charset;
import java.util.Scanner;


/**
 * @author WangTao
 */
public class NioClient {

    public void start(String nickname) throws IOException {
        SocketChannel channel = SocketChannel.open(new InetSocketAddress(
                "localhost", 8000));

        //接受服务器响应
        Selector selector = Selector.open();
        channel.configureBlocking(false);
        channel.register(selector, SelectionKey.OP_READ);
        new Thread(new NioClientHandler(selector)).start();


        Scanner scanner = new Scanner(System.in);
        while (scanner.hasNext()){
            String request = scanner.next();
            if (request != null && request.length() > 0){
                channel.write(
                        Charset.forName("UTF-8")
                                .encode(nickname + " : " + request));
            }
        }
    }

    public static void main(String[] args) throws IOException {
        NioClient nioClient = new NioClient();
        nioClient.start("client A");
    }
}
