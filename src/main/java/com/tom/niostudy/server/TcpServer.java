package com.tom.niostudy.server;
import	java.util.concurrent.ExecutorService;



import com.tom.niostudy.handler.ClientHandler;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.concurrent.Executors;

public class TcpServer {

    public static void main(String[] args) throws IOException {
        ServerSocket serverSocket = new ServerSocket(8890);
        System.out.println("服务器准备就绪");
        System.out.println("服务端:" + serverSocket.getInetAddress()
                +"===ip"+serverSocket.getLocalPort());
        for (;;){
            Socket socket = serverSocket.accept();
            ExecutorService executor = Executors.newCachedThreadPool();
            executor.execute(new ClientHandler(socket));
        }


    }
}
