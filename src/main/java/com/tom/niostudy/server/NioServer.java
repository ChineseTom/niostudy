package com.tom.niostudy.server;


import java.io.IOException;
import java.net.InetSocketAddress;
import java.nio.ByteBuffer;
import java.nio.channels.*;
import java.nio.charset.Charset;
import java.util.Iterator;
import java.util.Set;

/**
 * @author wangtao
 */
public class NioServer {

    public static void main(String[] args) throws IOException{
        NioServer nioServer = new NioServer();
        nioServer.start();
    }

    public void start() throws IOException {
        //1.创建一个Selector
        Selector selector = Selector.open();
        //2.通过ServerSocketChannel创建Channel通道
        ServerSocketChannel channel = ServerSocketChannel.open();
        //3.通过channel绑定端口
        channel.bind(new InetSocketAddress(8000));
        //4.设置channel为非阻塞模式
        channel.configureBlocking(false);
        //5.通过channel注册到selector上
        channel.register(selector, SelectionKey.OP_ACCEPT);
        System.out.println("服务器启动成功");
        //6.循环等待新接入的连接
        for (; ;) {
            //获取可用的Channels
            int readChannels = selector.select();
            if (readChannels == 0){
                continue;
            }
            Set<SelectionKey>  selectionKeys = selector.selectedKeys();
            Iterator<SelectionKey>  iterator = selectionKeys.iterator();

            while (iterator.hasNext()){
                SelectionKey selectionKey =  iterator.next();
                iterator.remove();
                //接入事件
                if (selectionKey.isAcceptable()){
                    acceptHandler(channel,selector);
                }
                //可读事件
                if (selectionKey.isReadable()) {
                    readHandler(selectionKey, selector);
                }
            }


        }
        //7.根据等待状态进行页面处理
    }

    private void acceptHandler(ServerSocketChannel serverSocketChannel,
                               Selector selector) throws IOException{
        /**
         * 如果要是接入事件，创建socketChannel
         */
        SocketChannel socketChannel = serverSocketChannel.accept();

        /**
         * 将socketChannel设置为非阻塞工作模式
         */
        socketChannel.configureBlocking(false);

        /**
         * 将channel注册到selector上，监听 可读事件
         */
        socketChannel.register(selector, SelectionKey.OP_READ);

        /**
         * 回复客户端提示信息
         */
        socketChannel.write(Charset.forName("utf-8").encode(
                "你与聊天室里其他人都不是朋友关系，请注意隐私安全"));

    }

    private void readHandler(SelectionKey selectionKey, Selector selector)
            throws IOException {
        /**
         * 要从 selectionKey 中获取到已经就绪的channel
         */
        SocketChannel socketChannel = (SocketChannel) selectionKey.channel();
        /**
         * 创建buffer
         */
        ByteBuffer byteBuffer = ByteBuffer.allocate(1024);

        StringBuffer sb = new StringBuffer();
        while (socketChannel.read(byteBuffer) > 0){
            /**
             * 切换buffer为读模式
             */
            byteBuffer.flip();

            /**
             * 读取buffer中的内容
             */
            sb.append(Charset.forName("UTF-8").decode(byteBuffer));
        }

        /**
         * 将channel再次注册到selector上，监听他的可读事件
         */
        socketChannel.register(selector, SelectionKey.OP_READ);

        /**
         * 将客户端发送的请求信息 广播给其他客户端
         */
        if (sb.length() > 0) {
            // 广播给其他客户端
            System.out.println(sb);
            broadCast(selector, socketChannel, sb.toString());
        }

    }

    /**
     * 广播给其他客户端
     */
    private void broadCast(Selector selector,
                           SocketChannel sourceChannel, String request) {
        /**
         * 获取到所有已接入的客户端channel
         */
        Set<SelectionKey> selectionKeySet = selector.keys();

        /**
         * 循环向所有channel广播信息
         */
        selectionKeySet.forEach(selectionKey -> {
            Channel targetChannel = selectionKey.channel();

            // 剔除发消息的客户端
            if (targetChannel instanceof SocketChannel
                    && targetChannel != sourceChannel) {
                try {
                    // 将信息发送到targetChannel客户端
                    ((SocketChannel) targetChannel).write(
                            Charset.forName("UTF-8").encode(request));
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        });
    }

}
