package com.tom.niostudy;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;


/**
 * @author WangTao
 */
@SpringBootApplication
public class NiostudyApplication {

    public static void main(String[] args) {
        SpringApplication.run(NiostudyApplication.class, args);
    }

}
